/**
* Validador de formulário
*  @author Anderson Martins
*/

function validar(){
	let nome = frmContato.nome.value
	let fone = frmContato.fone.value
	let email = frmContato.email.value
	
	// Validando o campo nome, se nome estiver vazio
	if (nome === ""){
		//Exibe um alerta
		alert("preencha o campo nome")
		// E coloca o cursor novamente no mesmo campo focus()		
		frmContato.nome.focus()
		return false
	} else if(fone === ""){
		//Exibe um alerta
		alert("preencha o campo fone")
		// E coloca o cursor novamente no mesmo campo focus()		
		frmContato.fone.focus()
		return false		
	} else if(email === ""){
		//Exibir um alerta
		alert("Por favor preencha o campo email")
		frmContato.email.focus()
		return false
	} else{
		// submetendo o formulário
		document.forms["frmContato"].submit()
	}
	//alert('teste de validação')
}