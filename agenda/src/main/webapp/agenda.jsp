<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="model.JavaBeans"%>
<%@ page import="java.util.ArrayList"%>

<%
ArrayList<JavaBeans> lista = (ArrayList<JavaBeans>) request.getAttribute("contatos");

// Teste de recebimento da lista
//for (int i=0; i<lista.size(); i++){
//	out.println(lista.get(i).getIdcon());
//	out.println(lista.get(i).getNome());
//	out.println(lista.get(i).getFone());
//	out.println(lista.get(i).getEmail());
//}

%>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<title>Agenda de Contatos</title>
<link rel="icon" href="imagens/favicon_phone_icone.png">
<link rel="stylesheet" href="style.css">
</head>
<body>
	
	<div class="tabela-cont">
		<h1>Agenda de Contatos</h1>
		<!-- Redirecionando para o doc novo.html -->
		<a href="novo.html" class="botao-1">Novo Contato</a>
		<a href="novo.html" class="botao-v">Relatórios</a>
		
		<!-- Criando a tabela do documento agenda -->
		<table id="tabela">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nome</th>
					<th>Fone</th>
					<th>E-mail</th>
					<th>Opções</th>
					<th>Excluir</th>
				</tr>
			</thead>
			
			<!-- Misturando scriplet com html -->
			<tbody>
				<!-- Sintaxe estranha -->
				<%for (int i=0; i<lista.size(); i++){ %>
					<tr>
						<td><%=lista.get(i).getIdcon()%></td>
						<td><%=lista.get(i).getNome()%></td>
						<td><%=lista.get(i).getFone()%></td>
						<td><%=lista.get(i).getEmail()%></td>
						<td><a class="botao-1" href="select?idcon=<%=lista.get(i).getIdcon()%>">Editar</a></a></td>
						<td><a class="botao-v" href="select?idcon=<%=lista.get(i).getIdcon()%>">Excluir</a></a></td>
					</tr>				
				<%} %>
			</tbody>
		</table>
	</div>

</body>
</html>

















