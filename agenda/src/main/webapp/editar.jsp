<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Edição de Contatos</title>
<link rel="icon" href="imagens/favicon_phone_icone.png">
<link rel="stylesheet" href="style.css">
</head>
<body>
	<div>
		<h1>Editar Contatos</h1>
		<form name="frmContato" action="update">
			<table>
				<tr>
					<!-- para esta caixa fizemos um estilo único -->
					<!-- readonly impede que o usuário edite o id do contato -->
					<td><input type="text" name="idcon" id="caixa-3" readonly
					value="<%out.print(request.getAttribute("idcon")); %>"></td>
				</tr>
				<tr>
					<td><input type="text" name="nome" class="caixa-1"
					value="<%out.print(request.getAttribute("nome")); %>"></td>
				</tr>
				<tr>
					<td><input type="text" name="fone" class="caixa-2"
					value="<%out.print(request.getAttribute("fone")); %>"></td>
				</tr>
				<tr>
					<td><input type="text" name="email" class="caixa-1"
					value="<%out.print(request.getAttribute("email")); %>"></td>
				</tr>
			</table>
			<input class="botao-1" type="button" value="Salvar" onclick="validar()">
		</form>
	</div>
	<script src="scripts/validador.js"></script>
</body>
</html>