package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DAO {
	/** Módulo de conexão **/
	// Parâmetros de conexão
	private String driver = "com.mysql.cj.jdbc.Driver";
	private String url = "jdbc:mysql://127.0.0.1:3306/dbagenda?useTimezone=true&serverTimezone=UTC";
	private String user = "root";
	private String password = null;
	// Método de conexão
	private Connection conectar() {
		Connection con = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, user, password);
			// O return faz a conexão
			return con;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return null;
		}
	}
	/* CRUD CREATE trazendo do banco */
	public void inserirContato(JavaBeans contato) {
		String create = "insert into contatos (nome, fone, email) values (?,?,?)"; // 3
		try {
			// Abrir a conexão
			Connection con = conectar(); // 3
			// Preparar a query para a execução no bancos variável pst para substituir a ???
			PreparedStatement pst = con.prepareStatement(create); // 3
			// Substituir os pontos de ??? com os dados do formulário da classe JavaBeans
			// substituindo interrogação (?) 1
			pst.setString(1, contato.getNome()); // 3
			// substituindo interrogação (?) 2
			pst.setString(2, contato.getFone()); // 3
			// substituindo interrogação (?) 3
			pst.setString(3, contato.getEmail()); // 3
			// Executando a query no banco
			pst.executeUpdate();
			// Encerrando a conexão com o banco
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	// teste de conexão com o banco
	/*
	 * public void testeConexao() { try { // Isso é o que eu espero que aconteça
	 * Connection con = conectar(); System.out.println(con); con.close(); } catch
	 * (Exception e) { // Se der algum erro este código de ve retornar
	 * System.out.println(e); } }
	 */
	/* CRUD READ - usando um vetor dinâmico em java */
	public ArrayList<JavaBeans> listarContatos() {
		// Criando um objetos para acessar a classe JavaBeans
		ArrayList<JavaBeans> contatos = new ArrayList<>();
		String read = "select * from contatos order by nome";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(read);
			// Novidade
			ResultSet rs = pst.executeQuery();
			// Laço de repetição while
			while (rs.next()) {
				// variaveis que recebem os dados que voltam do banco
				String idcon = rs.getString(1);
				String nome = rs.getString(2);
				String fone = rs.getString(3);
				String email = rs.getString(4);

				// popular todos os arraylist
				contatos.add(new JavaBeans(idcon, nome, fone, email));
			}
			con.close();
			return contatos;
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}		
	}	
	/* CRUD UPDATE selecionando um contato no banco */
	public void selecionarContato(JavaBeans contato) {
		String read2 = "select * from contatos where idcon = ?";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(read2);
			pst.setString(1, contato.getIdcon());
			ResultSet rs = pst.executeQuery();
			
			while(rs.next()) {
				// Configurando as variáveis JavaBeans
				contato.setIdcon(rs.getString(1));
				contato.setNome(rs.getString(2));
				contato.setFone(rs.getString(3));
				contato.setEmail(rs.getString(4));
			}
			con.close();			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	// Editar o contato
	public void alterarContato(JavaBeans contato) {
		String create = "update contatos set nome=?,fone=?,email=? where idcon=?";
		try {
			Connection con = conectar();
			PreparedStatement pst = con.prepareStatement(create);
			pst.setString(1, contato.getNome());
			pst.setString(2, contato.getFone());
			pst.setString(3, contato.getEmail());
			pst.setString(4, contato.getIdcon());
			pst.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
















