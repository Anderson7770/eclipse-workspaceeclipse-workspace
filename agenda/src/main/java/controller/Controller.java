package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.DAO;
import model.JavaBeans;

@WebServlet(urlPatterns = { "/Controller", "/main", "/ConexaoForm", "/select", "/update" })
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DAO dao = new DAO();
	JavaBeans contato = new JavaBeans();

	public Controller() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();
		System.out.println(action);
		// Se o conte�do do action conter a string "main", ela far� isso
		if (action.equals("/main")) {
			contatos(request, response); // 1
		} else if (action.equals("/insert")) {
			// este m�todo joga os dados para a camada do modelo
			novoContato(request, response); // 2
		} else if (action.equals("/select")) {
			// este m�todo joga os dados para a camada do modelo
			listarContato(request, response); // 2
		} else if (action.equals("/update")) {
			// este m�todo joga os dados para a camada do modelo
			editarContato(request, response); // 2
		} else {
			// Se tive alguma requisi��o n�o conhecida ele redireciona
			// para a p�gina do index.html
			response.sendRedirect("index.html");
		}
	}

	// Criar m�todo contatos() //1
	protected void contatos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Redirecionando para a p�gina agenda.jsp
		// Criando um m�todo que ir� receber os dados do JavaBeans
		ArrayList<JavaBeans> lista = dao.listarContatos();

		// Encaminhar a lista de dados ao documento agenda.jsp
		request.setAttribute("contatos", lista);
		// Redirecionando para o documento agenda.jsp
		RequestDispatcher rd = request.getRequestDispatcher("agenda.jsp");
		rd.forward(request, response);

		// teste de recebimento da lista que vem do banco
		// for (int i = 0; i<lista.size(); i++) {
		// System.out.println(lista.get(i).getIdcon());
		// System.out.println(lista.get(i).getNome());
		// System.out.println(lista.get(i).getFone());
		// System.out.println(lista.get(i).getEmail());
		// }
	}

	// Criar m�todo novoContato() //2
	protected void novoContato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Redirecionando para a p�gina
		// teste de envio de dados do formulario
		// System.out.println(request.getParameter("nome"));
		// System.out.println(request.getParameter("fone"));
		// System.out.println(request.getParameter("email"));
		// Jogando os valores nas vari�veis para o javabeans
		contato.setNome(request.getParameter("nome"));
		contato.setFone(request.getParameter("fone"));
		contato.setEmail(request.getParameter("email"));

		// Invocar o m�todo inserirContato() passando os dados contato
		dao.inserirContato(contato);
		// Redirecionar para o documento agenda.jsp
		response.sendRedirect("main");
	}

	// M�todo de editar contatos
	protected void listarContato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Recebimento do id do contato que ser� editado
		String idcon = request.getParameter("idcon");
		// Setando a variavel idcon
		contato.setIdcon(idcon);
		// Executar o m�todo selecionarContato() da classe (DAO)
		dao.selecionarContato(contato);
		// Setar os atributos do formulario com conteudo do javabeans
		request.setAttribute("idcon", contato.getIdcon());
		request.setAttribute("nome", contato.getNome());
		request.setAttribute("fone", contato.getFone());
		request.setAttribute("email", contato.getEmail());

		// Jogando estes dados no documento editar.jsp
		RequestDispatcher rd = request.getRequestDispatcher("editar.jsp");
		rd.forward(request, response);
	}

	// M�todo atualizar contatos
	protected void editarContato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Setar as vari�veis do javabens
		contato.setIdcon(request.getParameter("idcon"));
		contato.setNome(request.getParameter("nome"));
		contato.setFone(request.getParameter("fone"));
		contato.setEmail(request.getParameter("email"));
		// Executar o m�todo alterarContato
		dao.alterarContato(contato);
		
		// redirecionando para o doc. agenda.jsp (atualizando as altera��es)
		response.sendRedirect("main");
		
		// teste de recebimento de contatos
		System.out.println(request.getParameter("idcon"));
		System.out.println(request.getParameter("nome"));
		System.out.println(request.getParameter("fone"));
		System.out.println(request.getParameter("email"));
	}

}
