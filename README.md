# Instruções para repositórios git
eclipse-workspace
ID do Projeto: 25336091
O repositório para este projeto está vazio

You can get started by cloning the repository or start adding files to it with one of the following options.

Instruções de linha de comando

You can also upload existing files from your computer using the instructions below.
Configuração global do Git

git config --global user.name "Anderson Martins"
git config --global user.email "anderson77martins@outlook.com"

Criar um novo repositório

git clone https://gitlab.com/Anderson7770/eclipse-workspaceeclipse-workspace.git
cd eclipse-workspaceeclipse-workspace
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Anderson7770/eclipse-workspaceeclipse-workspace.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Anderson7770/eclipse-workspaceeclipse-workspace.git
git push -u origin --all
git push -u origin --tags
