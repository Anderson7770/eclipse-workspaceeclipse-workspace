package aplicacao;

import Pessoa_1.Pessoa;

public class Programa {

	public static void main(String[] args) {
		// Usando o objeto da classe Pessoa
		Pessoa p1 = new Pessoa(1, "Anderosn", "anderson@anderson.com");
		Pessoa p2 = new Pessoa(2, "Josy", "josy@josy.com");
		Pessoa p3 = new Pessoa(3, "Symone", "symone@symone.com");
		
		// Imprimindo na tela
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);

	}

}
