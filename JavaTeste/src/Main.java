// B�sico sequ�ncial

// %f = ponto flutuante
// %d = inteiro
// %s = texto ou string
// %n = quebra de linha

public class Main {

	public static void main(String[] args) {
		String nome = "Maria";
		int idade = 21;
		double renda = 20.001;
		
		System.out.printf("%s tem %d anos e ganha R$ %.2f", nome, idade, renda);
		
		System.out.println("Ol� Mundo!");
	}
}
